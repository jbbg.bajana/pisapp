/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelo.Carrito;
import java.util.List;

/**
 *
 * @author DELL
 */
public interface CarritoDao {
       public List<Carrito> consultarCarrito();
    public void agregarCarrito(Carrito carrito);
    public void eliminarCarrito(Carrito carrito); 
}
