/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import Dao.UsuarioDAOImpl;
import Dao.UsuarioDao;
import Modelo.Usuario;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;

/**
 *
 * @author keysw
 */
@Named(value = "mbInicioSesion")
@SessionScoped
public class mbInicioSesion implements Serializable {

    private Usuario usuario;
    private boolean sesionAbierta;
    
    public mbInicioSesion() {
        usuario = new Usuario();
        sesionAbierta = false;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public void validarUsuario(){
        UsuarioDao obj_UsuarioDao = new UsuarioDAOImpl();
        List<Usuario> lstUsuarios = obj_UsuarioDao.consultarUsuario();
        
        if(usuario != null){
            for(int i = 0; i < lstUsuarios.size() ;i++){
                if(usuario.getNickname().equals(lstUsuarios.get(i).getNickname())
                        && usuario.getContrasena().equals(lstUsuarios.get(i).getContrasena())){
                    sesionAbierta = true;
                    break;
                }
            }
        }
        
        //sesionAbierta = false;
    }
    
    public String sesionIniciada(){
        if(sesionAbierta){
            return "d-flex";
        }else{
            return "d-none";
        }
    }
    
    public String sesionNoIniciada(){
        if(sesionAbierta){
            return "d-none";
        }else{
            return "d-flex";
        }
    }
    
}
