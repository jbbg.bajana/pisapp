/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import Dao.CarritoDAOImpl;
import Dao.CarritoDao;
import Modelo.Carrito;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author DELL
 */


@Named(value = "mbCarrito")
@ViewScoped
public class mbCarrito implements Serializable{

    /**
     * Creates a new instance of mbCarrito
     */
    private Carrito obj_Carrito;
    
    public mbCarrito() {
        obj_Carrito = new Carrito();
    }

    public Carrito getObj_Carrito() {
        return obj_Carrito;
    }

    public void setObj_Carrito(Carrito obj_Carrito) {
        this.obj_Carrito = obj_Carrito;
    }
     public List<Carrito> getLstCarrito(){
        CarritoDao obj_CarritoDao = new CarritoDAOImpl();
        return obj_CarritoDao.consultarCarrito();
        
    }
    public void eliminarCarrito(){
        CarritoDao obj_CarritoDao = new CarritoDAOImpl();
            try {
                obj_CarritoDao.eliminarCarrito(obj_Carrito);
            } catch (Exception e) {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,"Msg",e.getMessage()));
            }           
        obj_Carrito = new Carrito();
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,"Msg","los datos fueron eliminados correctamente"));
    }
}
