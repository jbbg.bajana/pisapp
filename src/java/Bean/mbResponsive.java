/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import javax.inject.Named;
import java.io.Serializable;
import javax.faces.view.ViewScoped;

/**
 *
 * @author keysw
 */
@Named(value = "mbResponsive")
@ViewScoped
public class mbResponsive implements Serializable{

    private boolean abrirMenu;
    
    public mbResponsive(){
        abrirMenu = false;
    }
    
    public void mostrarMenu(){
        if(abrirMenu == false){
            abrirMenu = true;
        }else{
            abrirMenu = false;
        }
    }
    
    public String mostrarPanel(){
        if(abrirMenu){
            return "d-block position-absolute w-100 top-100 start-0 bg-light";
        }else{
            return "";
        }
    }
    
    public String mostrarBotones(){
        if(abrirMenu){
            return "d-block text-center";
        }else{
            return "";
        }
    }
    
}
