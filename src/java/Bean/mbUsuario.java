/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import Dao.UsuarioDAOImpl;
import Dao.UsuarioDao;
import Modelo.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author DELL
 */
@Named(value = "mbUsuario")
@ViewScoped
public class mbUsuario implements Serializable {

    /**
     * Creates a new instance of mbUsuario
     */
    
    private Usuario obj_Usuario;
    
    public mbUsuario() {
        obj_Usuario = new Usuario();
        
    }

    public Usuario getObj_Usuario() {
        return obj_Usuario;
    }

    public void setObj_Usuario(Usuario obj_Usuario) {
        this.obj_Usuario = obj_Usuario;
    }
    
    public List<Usuario> getLstUsuario(){
        UsuarioDao obj_UsuarioDao = new UsuarioDAOImpl();
        return obj_UsuarioDao.consultarUsuario();
        
    }
    
    public void agregarUsuario(){
        UsuarioDao obj_UsuarioDao = new UsuarioDAOImpl();
        try {
            obj_UsuarioDao.agregarUsuario(obj_Usuario);
            obj_Usuario = new Usuario();
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,"Msg","Los datos fueron ingresados correctamente"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,"Msg",e.getMessage()));
        }
       
        obj_Usuario = new Usuario();
    }
    
    
    public void modificarUsuario(){
        UsuarioDao obj_UsuarioDao = new UsuarioDAOImpl();
        try {
            obj_UsuarioDao.modificarUsuario(obj_Usuario);
            obj_Usuario = new Usuario();
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,"Msg","Los datos fueron modificados correctamente"));
        } catch (Exception e) {
             FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,"Msg",e.getMessage()));
        }

        obj_Usuario = new Usuario();
    }
    
    public void eliminarUsuario(){
        UsuarioDao obj_UsuarioDao = new UsuarioDAOImpl();
        try {
            obj_UsuarioDao.eliminarUsuario(obj_Usuario);
            obj_Usuario = new Usuario();
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,"Msg","Los datos fueron eliminados correctamente"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,"Msg",e.getMessage()));
        }

        obj_Usuario = new Usuario();
    }
    
}
